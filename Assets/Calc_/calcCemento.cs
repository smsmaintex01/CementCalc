﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class calcCemento : MonoBehaviour {

	public Dropdown TipoCemento;
	//public Dropdown Resistencia;
	//public Dropdown dosificasion;
	
	public Text aguaCant;
	public Text arenaCant;
	public Text cementoCant;
	public Text gravillaCant;

	public double agua;
	public double arena;
	public double cemento;
	public double gravilla;
	public double desperdicio;
	public double selCemento;

	//public GameObject cementoFunda;
	//public Animator anim;

	public InputField cantidadConstr;

	void Start(){

		//anim = GetComponent<Animator>();
		Calcular();

	}

	void Update()
	{

		if (cantidadConstr.text == "")
		{
			selCemento = 0;
		} 
		else
		{
			selCemento = double.Parse(cantidadConstr.text);
		}

	}

	public void Calcular(){

		if(TipoCemento.value == 0)
		{

			/*if(selCemento != 0)
			{
				anim.Play("Cemento_caer", -1, 0f);
			}*/
			desperdicio = (selCemento * 0.184) * 0.05;
			aguaCant.text = (selCemento * 0.184 + desperdicio).ToString("F2") + " M3 de agua.";
			desperdicio = (selCemento * 7.01) * 0.05;
			cementoCant.text = (selCemento * 7.01 + desperdicio).ToString("F2") + " bolsas de cemento.";
			desperdicio = (selCemento * 0.64) * 0.05;
			gravillaCant.text = (selCemento * 0.64 + desperdicio).ToString("F2") + " M3 de gravilla.";
			desperdicio = (selCemento * 0.51) * 0.05;
			arenaCant.text = (selCemento * 0.51 + desperdicio).ToString("F2") + " M3 de arena.";
		}
		else if(TipoCemento.value == 1)
		{
            /*if(selCemento != 0)
                        {
                            anim.Play("Cemento_caer", -1, 0f);
                        }*/
            desperdicio = (selCemento * 0.185) * 0.05;
			aguaCant.text = (selCemento * 0.185 + desperdicio).ToString("F2") + " M3 de agua.";
			desperdicio = (selCemento * 8.43) * 0.05;
			cementoCant.text = (selCemento * 8.43 + desperdicio).ToString("F2") + " bolsas de cemento.";
			desperdicio = (selCemento * 0.55) * 0.05;
			gravillaCant.text = (selCemento * 0.55 + desperdicio).ToString("F2") + " M3 de gravilla.";
			desperdicio = (selCemento * 0.54) * 0.05;
			arenaCant.text = (selCemento * 0.54 + desperdicio).ToString("F2") + " M3 de arena.";
		}
		else if(TipoCemento.value == 2)
		{
            /*if(selCemento != 0)
                        {
                            anim.Play("Cemento_caer", -1, 0f);
                        }*/
            desperdicio = (selCemento * 0.186) * 0.05;
			aguaCant.text = (selCemento * 0.186 + desperdicio).ToString("F2") + " M3 de agua.";
			desperdicio = (selCemento * 9.73) * 0.05;
			cementoCant.text = (selCemento * 9.73 + desperdicio).ToString("F2") + " bolsas de cemento.";
			desperdicio = (selCemento * 0.53) * 0.05;
			gravillaCant.text = (selCemento * 0.53 + desperdicio).ToString("F2") + " M3 de gravilla.";
			desperdicio = (selCemento * 0.52) * 0.05;
			arenaCant.text = (selCemento * 0.52 + desperdicio).ToString("F2") + " M3 de arena.";
		}
		else if(TipoCemento.value == 3)
		{

            /*if(selCemento != 0)
			{
				anim.Play("Cemento_caer", -1, 0f);
			}*/
            desperdicio = (selCemento * 0.187) * 0.05;
			aguaCant.text = (selCemento * 0.187 + desperdicio).ToString("F2") + " M3 de agua.";
			desperdicio = (selCemento * 11.50) * 0.05;
			cementoCant.text = (selCemento * 11.50 + desperdicio).ToString("F2") + " bolsas de cemento.";
			desperdicio = (selCemento * 0.51) * 0.05;
			gravillaCant.text = (selCemento * 0.51 + desperdicio).ToString("F2") + " M3 de gravilla.";
			desperdicio = (selCemento * 0.50) * 0.05;
			arenaCant.text = (selCemento * 0.50 + desperdicio).ToString("F2") + " M3 de arena.";
		}
		else if(TipoCemento.value == 4)
		{

            /*if(selCemento != 0)
			{
				anim.Play("Cemento_caer", -1, 0f);
			}*/
            desperdicio = (selCemento * 0.189) * 0.05;
			aguaCant.text = (selCemento * 0.189 + desperdicio).ToString("F2") + " M3 de agua.";
			desperdicio = (selCemento * 13.34) * 0.05;
			cementoCant.text = (selCemento * 13.34 + desperdicio).ToString("F2") + " bolsas de cemento.";
			desperdicio = (selCemento * 0.51) * 0.05;
			gravillaCant.text = (selCemento * 0.51 + desperdicio).ToString("F2") + " M3 de gravilla.";
			desperdicio = (selCemento * 0.184) * 0.05;
			arenaCant.text = (selCemento * 0.45 + desperdicio).ToString("F2") + " M3 de arena.";
		}

	}

}
