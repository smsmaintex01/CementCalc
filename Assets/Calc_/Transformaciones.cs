﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Transformaciones : MonoBehaviour {

	public double a;
	public double b;
	public double result;
	public InputField atxt;
	public Text resultadoTxt;
	
	public Dropdown ALong;
	public Dropdown BLong;
	public Dropdown Aarea;
	public Dropdown Barea;
	public Dropdown AVol;
	public Dropdown BVol;
	public Dropdown Atemp;
	public Dropdown Btemp;
	public Dropdown Apeso;
	public Dropdown Bpeso;
	public Dropdown Measure;
	
	public GameObject longitud;
	public GameObject area;
	public GameObject volumen;
	public GameObject temperatura;
	public GameObject peso;
	
	void Start(){

		//valores a iniciar la aplicacion
		result = 0;
		a = 0;
		b = 0;
	}

	void Update () {
		
		//Refrescar valores A y B en cada fotograma
		if (atxt.text == ""){
			a = 0;
		} else{
			a = float.Parse(atxt.text);
		}

		//Elegir entre longitud, area, temperatura, peso y volumen

		if(Measure.value == 0){
			longitud.SetActive(true);
			//area.SetActive(false);
			//volumen.SetActive(false);
			temperatura.SetActive(false);
			//peso.SetActive(false);
		} 
		/*else if(Measure.value == 1){
			longitud.SetActive(false);
			area.SetActive(true);
			volumen.SetActive(false);
			temperatura.SetActive(false);
			peso.SetActive(false);
		} 
		else if(Measure.value == 2){
			longitud.SetActive(false);
			area.SetActive(false);
			volumen.SetActive(true);
			temperatura.SetActive(false);
			peso.SetActive(false);
		} */
		else if(Measure.value == 1){
			longitud.SetActive(false);
			//area.SetActive(false);
			//volumen.SetActive(false);
			temperatura.SetActive(true);
			//peso.SetActive(false);
		} /*
		else if(Measure.value == 4){
			longitud.SetActive(true);
			area.SetActive(false);
			volumen.SetActive(false);
			temperatura.SetActive(false);
			peso.SetActive(true);
		}*/
	}

	public void Calcular(){
		if (ALong.value == 0 && Measure.value == 0)
		{ //Kilometros
			if (BLong.value == 0)
			{
				b = a;
				resultadoTxt.text = b.ToString("F3") + " Kilómetros";
			}
			else if (BLong.value == 1)
			{
				b = (a * 1000) / 1; //Km a M 
				resultadoTxt.text = b + " Metros";
			}
			else if (BLong.value == 2)
			{
				b = (a * 100000) / 1; //cm a mm
				resultadoTxt.text = b + " Centímetros";
			}
			else if (BLong.value == 3)
			{
				b = (a * 1000000) / 1; //m a mm
				resultadoTxt.text = b + " Milímetros";
			}
			else if (BLong.value == 4)
			{
				b = (a / 1.609); //km a millas
				resultadoTxt.text = b.ToString("F2") + " Millas";
			}
			else if (BLong.value == 5)
			{
				b = a * 1093.61; // km a Yardas
				resultadoTxt.text = b.ToString("F2") + " Yardas";
			}
			else if (BLong.value == 6)
			{
				b = a * 3280.83; //km a Pies
				resultadoTxt.text = b.ToString("F2") + " Pies";
			}
			else if (BLong.value == 7)
			{
				b = a * 39370.07; // km a pulgadas
				resultadoTxt.text = b.ToString("F2") + " Pulgadas";
			}
		}
		else if (ALong.value == 1 && Measure.value == 0)
		{ //Metros
			if (BLong.value == 0)
			{
				b = (a * 1) / 1000; //metros a KM
				resultadoTxt.text = b + " Kilómetros";
			}
			else if (BLong.value == 1)
			{
				b = a; // metros a metros, lol
				resultadoTxt.text = b + " Metros";
			}
			else if (BLong.value == 2)
			{
				b = (a * 100) / 1; //cm a mm
				resultadoTxt.text = b + " Centímetros";
			}
			else if (BLong.value == 3)
			{
				b = (a * 1000) / 1; //m a mm
				resultadoTxt.text = b + " Milímetros";
			}
			else if (BLong.value == 4)
			{
				b = a * 0.000621371; //km a millas
				resultadoTxt.text = b.ToString("F7") + " Millas";
			}
			else if (BLong.value == 5)
			{
				b = a * 1.09361; // km a Yardas
				resultadoTxt.text = b.ToString("F2") + " Yardas";
			}
			else if (BLong.value == 6)
			{
				b = a * 3.28084; //km a Pies
				resultadoTxt.text = b.ToString("F2") + " Pies";
			}
			else if (BLong.value == 7)
			{
				b = a * 39.37008; // km a pulgadas
				resultadoTxt.text = b.ToString("F2") + " Pulgadas";
			}
		}
		else if (ALong.value == 2 && Measure.value == 0)
		{ //Centimetros
			if (BLong.value == 0)
			{
				b = a / 100000; //centimetros a kilometros
				resultadoTxt.text = b + " Kilómetros";
			}
			else if (BLong.value == 1)
			{
				b = a / 100;
				resultadoTxt.text = b + " Metros";
			}
			else if (BLong.value == 2)
			{
				b = a; //cm a mm
				resultadoTxt.text = b + " Centímetros";
			}
			else if (BLong.value == 3)
			{
				b = (a * 10) / 1; //m a mm
				resultadoTxt.text = b + " Milímetros";
			}
			else if (BLong.value == 4)
			{
				b = a / 160934; //km a millas
				resultadoTxt.text = b.ToString("F5") + " Millas";
			}
			else if (BLong.value == 5)
			{
				b = a / 91.44; // km a Yardas
				resultadoTxt.text = b.ToString("F2") + " Yardas";
			}
			else if (BLong.value == 6)
			{
				b = a / 30.48; //km a Pies
				resultadoTxt.text = b.ToString("F2") + " Pies";
			}
			else if (BLong.value == 7)
			{
				b = a / 2.54; // km a pulgadas
				resultadoTxt.text = b.ToString("F2") + " Pulgadas";
			}
		}
		else if (ALong.value == 3 && Measure.value == 0)
		{ //Milimetros
			if (BLong.value == 0)
			{
				b = a / 1000000;
				resultadoTxt.text = b + " Kilómetros";
			}
			else if (BLong.value == 1)
			{
				b = a * 0.001;
				resultadoTxt.text = b + " Metros";
			}
			else if (BLong.value == 2)
			{
				b = a * 0.1;
				resultadoTxt.text = b + " Centímetros";
			}
			else if (BLong.value == 3)
			{
				b = a; //m a mm
				resultadoTxt.text = b + " Milímetros";
			}
			else if (BLong.value == 4)
			{
				b = (a * 0.000621) / 1000; //km a millas
				resultadoTxt.text = b.ToString("F5") + " Millas";
			}
			else if (BLong.value == 5)
			{
				b = (a * 1.09361) / 1000; // km a Yardas
				resultadoTxt.text = b.ToString("F2") + " Yardas";
			}
			else if (BLong.value == 6)
			{
				b = (a * 3.28084) / 1000; //km a Pies
				resultadoTxt.text = b.ToString("F3") + " Pies";
			}
			else if (BLong.value == 7)
			{
				b = (a * 39.3701) / 1000; // km a pulgadas
				resultadoTxt.text = b.ToString("F2") + " Pulgadas";
			}
		}
		else if (ALong.value == 4 && Measure.value == 0)
		{ //Milla
			if (BLong.value == 0)
			{
				b = (a * 1609.34) / 1000;
				resultadoTxt.text = b + " Kilómetros";
			}
			else if (BLong.value == 1)
			{
				b = (a * 160934) / 100;
				resultadoTxt.text = b + " Metros";
			}
			else if (BLong.value == 2)
			{
				b = (a * 160934) / 1;
				resultadoTxt.text = b + " Centímetros";
			}
			else if (BLong.value == 3)
			{
				b = (a * 1609344) / 0.01;
				resultadoTxt.text = b + " Milímetros";
			}
			else if (BLong.value == 4)
			{
				b = a;
				resultadoTxt.text = b.ToString("F5") + " Millas";
			}
			else if (BLong.value == 5)
			{
				b = (a * 17600) / 10;
				resultadoTxt.text = b.ToString("F2") + " Yardas";
			}
			else if (BLong.value == 6)
			{
				b = (a * 528000) / 100; //km a Pies
				resultadoTxt.text = b.ToString("F3") + " Pies";
			}
			else if (BLong.value == 7)
			{
				b = (a * 633600) / 10; // km a pulgadas
				resultadoTxt.text = b.ToString("F2") + " Pulgadas";
			}
		}
		else if (ALong.value == 5 && Measure.value == 0)
		{ //Yarda
			if (BLong.value == 0)
			{
				b = (a * 0.9144) / 1000;
				resultadoTxt.text = b + " Kilómetros";
			}
			else if (BLong.value == 1)
			{
				b = (a * 914.4) / 1000;
				resultadoTxt.text = b + " Metros";
			}
			else if (BLong.value == 2)
			{
				b = (a * 91440) / 1000;
				resultadoTxt.text = b + " Centímetros";
			}
			else if (BLong.value == 3)
			{
				b = (a * 914400) / 1000;
				resultadoTxt.text = b + " Milímetros";
			}
			else if (BLong.value == 4)
			{
				b = (a * 0.568182) / 1000;
				resultadoTxt.text = b.ToString("F5") + " Millas";
			}
			else if (BLong.value == 5)
			{
				b = a;
				resultadoTxt.text = b.ToString("F2") + " Yardas";
			}
			else if (BLong.value == 6)
			{
				b = (a * 3000) / 1000; //km a Pies
				resultadoTxt.text = b.ToString("F3") + " Pies";
			}
			else if (BLong.value == 7)
			{
				b = (a * 36000) / 1000; // km a pulgadas
				resultadoTxt.text = b.ToString("F2") + " Pulgadas";
			}
		}
		else if (ALong.value == 6 && Measure.value == 0)
		{ //Pie
			if (BLong.value == 0)
			{
				b = (a * 0.3048) / 1000;
				resultadoTxt.text = b + " Kilómetros";
			}
			else if (BLong.value == 1)
			{
				b = (a * 304.8) / 1000;
				resultadoTxt.text = b + " Metros";
			}
			else if (BLong.value == 2)
			{
				b = (a * 3048) / 100;
				resultadoTxt.text = b + " Centímetros";
			}
			else if (BLong.value == 3)
			{
				b = (a * 304800) / 1000;
				resultadoTxt.text = b + " Milímetros";
			}
			else if (BLong.value == 4)
			{
				b = (a * 0.189394) / 1000;
				resultadoTxt.text = b.ToString("F5") + " Millas";
			}
			else if (BLong.value == 5)
			{
				b = (a * 333.333) / 1000;
				resultadoTxt.text = b.ToString("F2") + " Yardas";
			}
			else if (BLong.value == 6)
			{
				b = a;
				resultadoTxt.text = b.ToString("F3") + " Pies";
			}
			else if (BLong.value == 7)
			{
				b = (a * 12000) / 1000; // km a pulgadas
				resultadoTxt.text = b.ToString("F2") + " Pulgadas";
			}
		}
		else if (ALong.value == 7 && Measure.value == 0)
		{ //Pulgadas
			if (BLong.value == 0)
			{
				b = (a * 0.0254) / 1000;
				resultadoTxt.text = b + " Kilómetros";
			}
			else if (BLong.value == 1)
			{
				b = (a * 25.4) / 1000;
				resultadoTxt.text = b + " Metros";
			}
			else if (BLong.value == 2)
			{
				b = (a * 2540) / 1000;
				resultadoTxt.text = b + " Centímetros";
			}
			else if (BLong.value == 3)
			{
				b = (a * 25400) / 1000;
				resultadoTxt.text = b + " Milímetros";
			}
			else if (BLong.value == 4)
			{
				b = (a * 0.0157828) / 1000;
				resultadoTxt.text = b.ToString("F5") + " Millas";
			}
			else if (BLong.value == 5)
			{
				b = (a * 27.7778) / 1000;
				resultadoTxt.text = b.ToString("F2") + " Yardas";
			}
			else if (BLong.value == 6)
			{
				b = (a * 83.3333) / 1000;
				resultadoTxt.text = b.ToString("F3") + " Pies";
			}
			else if (BLong.value == 7)
			{
				b = a; // km a pulgadas
				resultadoTxt.text = b.ToString("F2") + " Pulgadas";
			}
		}

		/*
		**------------------------------------
		** Esta e la Secicon de la Temperatura
		**------------------------------------
		*/

		if (Atemp.value == 0 && Measure.value == 1){
			if(Btemp.value == 0){ //celsius a Celsius
				b = a;
				resultadoTxt.text = b.ToString("F2") + " ºC";
			} else if(Btemp.value == 1){//Cesius a Fahrenheit
				b = (a * 1.8) + 32; 
				resultadoTxt.text = b.ToString("F2") + " ºF";
			} else if(Btemp.value == 2){//Celsius a Kelvin
				b = a + 273;
				resultadoTxt.text = b.ToString("F2") + " ºK";
			}
		}else if (Atemp.value == 1 && Measure.value ==1){
			if (Btemp.value == 0){ //Fahrenheit a Celsius{
				b = (a - 32) / 1.8;
				resultadoTxt.text = b.ToString("F2") + "ºC";
			}else if(Btemp.value == 1){// Fahrnheit a Fahrenheit
				b = a;
				resultadoTxt.text = b.ToString("F2") + "ºF";
			}else if(Btemp.value == 2){ //Fahrengeit a Kelvin
				b = (a - 32) / 1.8;
				b += 273;
				resultadoTxt.text = b.ToString("F2") + "ºK";
			}
		}else if (Atemp.value == 2 && Measure.value ==1){
			if (Btemp.value == 0){ //Kelvin a Celsius{
				b = a - 273;
				resultadoTxt.text = b.ToString("F2") + "ºC";
			}else if(Btemp.value == 1){// Kelvin a Fahrenheit
				b = 1.8*(a - 273)+32;
				resultadoTxt.text = b.ToString("F2") + "ºF";
			}else if(Btemp.value == 2){ //Kelvin a Kelvin
				b = a;
				resultadoTxt.text = b.ToString("F2") + "ºK";
			}
		}
	}


}
