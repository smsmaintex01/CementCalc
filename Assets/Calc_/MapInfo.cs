﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MapInfo : MonoBehaviour {

	public Text component;
	public string value;

	void OnMouseDown()
	{
		component.text = value.Replace("\\n", Environment.NewLine);
	}
}
