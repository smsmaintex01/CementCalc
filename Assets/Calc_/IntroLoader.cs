﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroLoader : MonoBehaviour 
{

	public GameObject contrato;

	public void noAcepto()
	{
		
		#if UNITY_EDITOR
    		UnityEditor.EditorApplication.isPlaying = false;
  		#endif
    }

	public void Acepto()
	{
		StartCoroutine("AceptoAccion");
	}

	public IEnumerator AceptoAccion() 
	{
		contrato.SetActive(false);
		yield return new WaitForSeconds(3);
		SceneManager.LoadScene(1, LoadSceneMode.Single);
	}
	
}
