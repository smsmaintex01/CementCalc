﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPanelChanger : MonoBehaviour
{
    public GameObject introPanel;
    public GameObject FichaTecnicaPanel;
    public GameObject CementoBlanco;
    public GameObject CementoGrisGeneral;
    public GameObject CementoGrisEstructural;

    void Start()
    {
        introPanel.SetActive(true);
        FichaTecnicaPanel.SetActive(false);
        CementoBlanco.SetActive(false);
        CementoGrisGeneral.SetActive(false);
        CementoGrisEstructural.SetActive(false);

    }

    public void irAFichaTecnica()
    {
        introPanel.SetActive(false);
        FichaTecnicaPanel.SetActive(true);
        CementoBlanco.SetActive(false);
        CementoGrisGeneral.SetActive(false);
        CementoGrisEstructural.SetActive(false);

    }

    public void irAtras()
    {
        introPanel.SetActive(true);
        FichaTecnicaPanel.SetActive(false);
        CementoBlanco.SetActive(false);
        CementoGrisGeneral.SetActive(false);
        CementoGrisEstructural.SetActive(false);
    }

    public void IrACementoBlanco()
    {
        introPanel.SetActive(true);
        FichaTecnicaPanel.SetActive(false);
        CementoBlanco.SetActive(true);
        CementoGrisGeneral.SetActive(false);
        CementoGrisEstructural.SetActive(false);
    }

    public void IrACementoGrisGeneral()
    {
        introPanel.SetActive(true);
        FichaTecnicaPanel.SetActive(false);
        CementoBlanco.SetActive(false);
        CementoGrisGeneral.SetActive(true);
        CementoGrisEstructural.SetActive(false);
    }

    public void IrACementoGrisEstructural()
    {
        introPanel.SetActive(true);
        FichaTecnicaPanel.SetActive(false);
        CementoBlanco.SetActive(false);
        CementoGrisGeneral.SetActive(false);
        CementoGrisEstructural.SetActive(true);
    }

    public void goBack()
    {
        introPanel.SetActive(false);
        FichaTecnicaPanel.SetActive(true);
        CementoBlanco.SetActive(false);
        CementoGrisGeneral.SetActive(false);
        CementoGrisEstructural.SetActive(false);

    }
}
