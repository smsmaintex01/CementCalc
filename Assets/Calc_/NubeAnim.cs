﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NubeAnim : MonoBehaviour {

	private Rigidbody2D rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();	
	}
	
	// Update is called once per frame
	void Update () {
		rb.velocity = new Vector2(0.5f, 0);
		}
}
